'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// $FlowFixMe


// Types for flow syntax-checking
Math.degrees = function (radians) {
  return radians * 180 / Math.PI;
};

/**
Component which processes device orientation events.
*/

var Tilt = function (_Component) {
  _inherits(Tilt, _Component);

  function Tilt() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Tilt);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Tilt.__proto__ || Object.getPrototypeOf(Tilt)).call.apply(_ref, [this].concat(args))), _this), _this.hasDeviceOrientation = window.DeviceOrientationEvent, _this.deviceOrientation = function (e) {
      if (_this.props.onTilt) {
        _this.props.onTilt(_this.handleOrientation(e));
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Tilt, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate() {
      return false;
    }
  }, {
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (this.hasDeviceOrientation) {
        window.addEventListener('deviceorientation', this.deviceOrientation, false);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.hasDeviceOrientation) {
        window.removeEventListener('deviceorientation', this.deviceOrientation, false);
      }
    }

    // $FlowFixMe

  }, {
    key: 'handleOrientation',
    value: function handleOrientation(event) {
      // those angles are in degrees
      var beta = event.beta;
      var gamma = event.gamma;

      // JS math works in radians
      var betaR = beta / 180 * Math.PI;
      var gammaR = gamma / 180 * Math.PI;
      var spinR = Math.atan2(Math.cos(betaR) * Math.sin(gammaR), Math.sin(betaR));

      // convert back to degrees
      var spin = spinR * 180 / Math.PI;

      if (spin >= 90) {
        spin = 90;
      } else if (spin <= -90) {
        spin = -90;
      }

      return Math.round(spin);
    }
  }, {
    key: 'render',
    value: function render() {
      return null;
    }
  }]);

  return Tilt;
}(_react.Component);

exports.default = Tilt;